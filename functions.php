<?php

function imageLocation(): array
{
    return [
        "left" => random_int(0, (int)$_SESSION["width"] - ICON_WIDTH),
        "top" => random_int(0, (int)$_SESSION["height"] - ICON_HEIGHT)
    ];
}

function defaultImgLocation(): array
{
    $iconsLocation = [];
    foreach (ICONS as $icon) {
        for ($i = 0; $i < DEFAULT_ICON_QUANTITY; $i++) {
            $iconsLocation[$icon][] = imageLocation();
        }
    }

    return $iconsLocation;
}

function moveImages(array $imageLocation): array
{
    foreach ($imageLocation as &$icons) {
        foreach ($icons as &$icon) {
            $icon['left'] = moveLength($icon['left'], true);
            $icon['top'] = moveLength($icon['top']);
        }
    }

    return $imageLocation;
}

/**
 * $width - true => width
 * $width - false => height
 */
function moveLength(int $default, $width = false): int
{
    $maxValue = (int)$_SESSION["height"] - ICON_HEIGHT;

    if ($width) {
        $maxValue = (int)$_SESSION["width"] - ICON_WIDTH;
    }

    $mask = [-3*ICON_STEP, -ICON_STEP, 0, ICON_STEP, 0.9*ICON_STEP, 1.8*ICON_STEP, 2*ICON_STEP];
    $int = random_int(0, count($mask) - 1);

    $addition = $default + $mask[$int];



    return ((($addition >= 0) && ($addition <= $maxValue)) ? $addition : ($default - $mask[$int]));
}

function showIcons(array $imageLocation): string
{
    $html = "";

    foreach ($imageLocation as $path => $values) {
        foreach ($values as $value) {
            $html .= "<img  
                src='". $path . "' 
                style='position:absolute; left:" . $value['left'] . "; top:" . $value['top'] . "; height: " . ICON_HEIGHT . "; width: " . ICON_WIDTH . ";' >";
        }
    }

    unset($imageLocation, $path, $value, $values);

    return $html;
}

function deleteAction(array $imageLocation): array
{
    if (!empty($imageLocation[ICONS[0]])) {
        $firstIC = $imageLocation[ICONS[0]];
    }

    if (!empty($imageLocation[ICONS[1]])) {
        $secondIC = $imageLocation[ICONS[1]];
    }

    if (!empty($imageLocation[ICONS[2]])) {
        $thirdIC = $imageLocation[ICONS[2]];
    }

    /**
     * FIRST
     */
    if (!empty($firstIC) && !empty($secondIC)) {
        foreach ($firstIC as $ficKey => $ficValue) {
            $ficLeft[$ficKey] = $ficValue['left'];
            $ficTop[$ficKey] = $ficValue['top'];
        }

        foreach ($secondIC as $sicKey => $sicValue) {
            $sicLeft[$sicKey] = $sicValue['left'];
            $sicTop[$sicKey] = $sicValue['top'];
        }

        $countF = count($ficLeft);
        $countS = count($sicLeft);

        if ($countF >= $countS) {
            $collision = [];
            foreach ($ficLeft as $ficlKey => $ficlVal) {
                foreach ($sicLeft as $siclKey => $siclVal) {
                    if (abs($ficlVal - $siclVal) < (ICON_WIDTH)) {
                        $collision[] = ['f' => $ficlKey, 's' => $siclKey];
                    }
                }
            }
        } else {
            $collision = [];
            foreach ($sicLeft as $siclKey => $siclVal) {
                foreach ($ficLeft as $ficlKey => $ficlVal) {
                    if (abs($siclVal - $ficlVal) < (ICON_WIDTH)) {
                        $collision[] = ['f' => $ficlKey, 's' => $siclKey];
                    }
                }
            }
        }

        if (!empty($collision)) {
            foreach ($collision as $item) {
                if (abs((int)$firstIC[$item['f']]['top'] - (int)$secondIC[$item['s']]['top']) < (ICON_HEIGHT)) {
                    $firstIC[] = $secondIC[$item['s']];
                    unset($secondIC[$item['s']]);
                }
            }
        }
    }

    /**
     * SECOND
     */
    if (!empty($thirdIC) && !empty($secondIC)) {
        foreach ($thirdIC as $ticKey => $ticValue) {
            $ticLeft[$ticKey] = $ticValue['left'];
            $ticTop[$ticKey] = $ticValue['top'];
        }

        foreach ($secondIC as $sicKey => $sicValue) {
            $sicLeft[$sicKey] = $sicValue['left'];
            $sicTop[$sicKey] = $sicValue['top'];
        }

        $countT = count($ticLeft);
        $countS = count($sicLeft);

        if ($countT >= $countS) {
            $collision = [];
            foreach ($ticLeft as $ticlKey => $ticlVal) {
                foreach ($sicLeft as $siclKey => $siclVal) {
                    if (abs($ticlVal - $siclVal) < (ICON_WIDTH)) {
                        $collision[] = ['t' => $ticlKey, 's' => $siclKey];
                    }
                }
            }
        } else {
            $collision = [];
            foreach ($sicLeft as $siclKey => $siclVal) {
                foreach ($ticLeft as $ticlKey => $ticlVal) {
                    if (abs($siclVal - $ticlVal) < (ICON_WIDTH)) {
                        $collision[] = ['t' => $ticlKey, 's' => $siclKey];
                    }
                }
            }
        }

        if (!empty($collision)) {
            foreach ($collision as $item) {
                if (abs((int)$thirdIC[$item['t']]['top'] - (int)$secondIC[$item['s']]['top']) < (ICON_HEIGHT)) {
                    $secondIC[] = $thirdIC[$item['t']];
                    unset($thirdIC[$item['t']]);
                }
            }
        }
    }

//    /**
//     * THIRD
//     */
    if (!empty($firstIC) && !empty($thirdIC)) {
        foreach ($firstIC as $ficKey => $ficValue) {
            $ficLeft[$ficKey] = $ficValue['left'];
            $ficTop[$ficKey] = $ficValue['top'];
        }

        foreach ($thirdIC as $ticKey => $ticValue) {
            $ticLeft[$ticKey] = $ticValue['left'];
            $ticTop[$ticKey] = $ticValue['top'];
        }

        $countF = count($ficLeft);
        $countT = count($ticLeft);

        if ($countF >= $countT) {
            $collision = [];
            foreach ($ficLeft as $ficlKey => $ficlVal) {
                foreach ($ticLeft as $ticlKey => $ticlVal) {
                    if (abs($ficlVal - $ticlVal) < (ICON_WIDTH)) {
                        $collision[] = ['f' => $ficlKey, 't' => $ticlKey];
                    }
                }
            }
        } else {
            $collision = [];
            foreach ($ticLeft as $ticlKey => $ticlVal) {
                foreach ($ficLeft as $ficlKey => $ficlVal) {
                    if (abs($ticlVal - $ficlVal) < (ICON_WIDTH)) {
                        $collision[] = ['f' => $ficlKey, 't' => $ticlKey];
                    }
                }
            }
        }

        if (!empty($collision)) {
            foreach ($collision as $item) {
                if (abs((int)$firstIC[$item['f']]['top'] - (int)$thirdIC[$item['t']]['top']) < (ICON_HEIGHT)) {
                    $thirdIC[] = $firstIC[$item['f']];
                    unset($firstIC[$item['f']]);
                }
            }
        }
    }

    $imageLocation = [];
    $imageLocation[ICONS[0]] = $firstIC;
    $imageLocation[ICONS[1]] = $secondIC;
    $imageLocation[ICONS[2]] = $thirdIC;

    unset(
        $firstIC,
        $secondIC,
        $thirdIC,
        $collision,
        $item,
        $ficlKey,
        $ficValue,
        $ficKey,
        $ficLeft,
        $ficTop,
        $sicKey,
        $sicValue,
        $sicLeft,
        $sicTop,
        $countF,
        $countS,
        $ficlVal,
        $siclKey,
        $siclVal,
        $ticlKey,
        $ticlVal,
        $ticValue,
        $ticKey
    );

    return $imageLocation;
}

function cover(array $imageLocation): string
{
    $html = '';

    foreach ($imageLocation as $values) {
        foreach ($values as $value) {
            $html .= "<img  
                src='" . "icons/white.png" . "' 
                style='position:absolute; left:" . $value['left'] . "; top:" . $value['top'] . "; height: " . ICON_HEIGHT . "; width: " . ICON_WIDTH . ";' >";
        }
    }

    unset($value, $values);

    return $html;
}