<?php

const DEFAULT_ICON_QUANTITY = 10;
const ICON_HEIGHT = 50;
const ICON_WIDTH = 50;

const ICON_STEP = 5;

const ICONS = [
    "icons/tw.png",
    "icons/nikita.png",
    "icons/elena.png",
];

const GAME_DATA_FILES = "gamesData/";