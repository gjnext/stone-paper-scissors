<?php
ini_set('max_execution_time', 0);

session_start();


include "config.php";
include "functions.php";

$iterator = 1;
game();

function game() {

    $gameData = [];

    if (file_exists(GAME_DATA_FILES . $_SESSION["gameId"])) {
        $gameData = json_decode(file_get_contents(GAME_DATA_FILES . $_SESSION["gameId"]), true);
    }

    if (empty($gameData['imageLocation'])) {
        $gameData['imageLocation'] = defaultImgLocation();
    }

    if (empty($gameData['startTime'])) {
        $gameData['startTime'] = time();
    }

    echo cover($gameData['imageLocation']);

    $gameData['imageLocation'] = moveImages($gameData['imageLocation']);

    $gameData['imageLocation'] = deleteAction($gameData['imageLocation']);

    file_put_contents(GAME_DATA_FILES . $_SESSION["gameId"], json_encode($gameData));

    echo showIcons($gameData['imageLocation']);

    if (empty($gameData['imageLocation'][ICONS[2]])) {
        exit("Game over (Елена проиграла).");
    }
    if (empty($gameData['imageLocation'][ICONS[1]])) {
        exit("Game over (Никита проиграл).");
    }

    if (empty($gameData['imageLocation'][ICONS[0]])) {
        exit("Game over (теория вероятностей проиграла).");
    }

    if (time() - $gameData['startTime'] > 600) {
        exit("Game over (Timeout).");
    }

    usleep(90000);

    game();
}