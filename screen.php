<?php

session_start();

$url = $_SERVER['REQUEST_URI'];
$parts = parse_url( $url );
parse_str( $parts['query'] , $query);

$_SESSION['width'] = $query['width'];
$_SESSION['height'] = $query['height'];
$_SESSION['gameId'] = random_int(1, 10000);

header('Location: /game.php');